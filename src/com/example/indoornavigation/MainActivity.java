package com.example.indoornavigation;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;
import java.util.ArrayList;


public class MainActivity extends Activity {

	private Spinner source,destination;
	
	private String value1,value2;
	
	private Button submit;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		addItemsOnSpinner2();
		addListenerOnButton();
		addListenerOnSpinnerItemSelection();
		
	}

	public void addItemsOnSpinner2() {
		// TODO Auto-generated method stub
		
		destination=(Spinner) findViewById(R.id.spinner2);
		List<String> list= new ArrayList<String>();
		list.add("Lima Pantry");
		list.add("Rome Pantry");
		list.add("Copper");
		list.add("Aspen");
		list.add("Snowmass");
		list.add("Telluride");
		list.add("Lunch Room");
		list.add("Wolfcreek");
		list.add("Durango");
		
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		destination.setAdapter(dataAdapter);
		
	}
	
public void addListenerOnSpinnerItemSelection(){
		
		source = (Spinner) findViewById(R.id.spinner1);
		source.setOnItemSelectedListener(new CustomOnItemSelectedListener());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		
		return true;
	}
 
	public void addListenerOnButton() {

		source = (Spinner) findViewById(R.id.spinner1);
		destination = (Spinner) findViewById(R.id.spinner2);
		
		submit = (Button) findViewById(R.id.button1);

		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				
				value1=String.valueOf(source.getSelectedItem());
				value2=String.valueOf(destination.getSelectedItem());
				
				if(value1.equals("Lima Pantry")&&value2.equals("Lunch Room"))
				{
					Intent first_dir= new Intent(MainActivity.this,FirstInfo.class);
					startActivity(first_dir);
				}
				else if(value1.equals("Wolfcreek")&&value2.equals("Snowmass"))
				{  
					Intent second_dir= new Intent(MainActivity.this,SecondInfo.class);
					startActivity(second_dir);
				}
				else if(value1.equals("Telluride")&&value2.equals("Copper"))
				{  
					Intent third_dir= new Intent(MainActivity.this,ThirdInfo.class);
					startActivity(third_dir);
				}
				Toast.makeText(MainActivity.this,
						"OnClickListener : " + 
						"\nselected source : " + String.valueOf(source.getSelectedItem()) +
						"\nSelected destination : " + String.valueOf(destination.getSelectedItem()),
						Toast.LENGTH_SHORT).show();
			
			}
				
		});
	}
}

	
				
				
				
				
				