package com.example.indoornavigation;

import java.util.Locale;

import android.app.Activity;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class RouteThird extends Activity implements OnInitListener{
	TextView val;
	 Button textspeech;
	 private TextToSpeech tts;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.firstinfo);
		val=(TextView) findViewById(R.id.textView1);
		
		View b = findViewById(R.id.textView1);
		b.setVisibility(View.INVISIBLE);
		
		tts=new TextToSpeech(getApplicationContext(), this);
		
		
		Button textspeech=(Button) findViewById(R.id.button1);
		
		
textspeech.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				View b = findViewById(R.id.textView1);
				b.setVisibility(View.VISIBLE);
				 speakOut();
			    
				//speakOut();
			}
		});
	}
	@Override
	public void onInit(int status) {
		// TODO Auto-generated method stub
		 if(status != TextToSpeech.ERROR){
		        tts.setLanguage(Locale.getDefault());
		    }
		
	}
	private void speakOut() {
		 
        String text = val.getText().toString();
 
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }
	
		
	}

