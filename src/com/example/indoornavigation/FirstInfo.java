package com.example.indoornavigation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class FirstInfo extends Activity {
	
	 Button map,route;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.info);
		
		map=(Button)findViewById(R.id.button1);
		route=(Button) findViewById(R.id.button2);
		
		map.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent map=new Intent(FirstInfo.this,MapFirst.class);
				startActivity(map);
				
			}
		});
				
		route.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent route=new Intent(FirstInfo.this,RouteFirst.class);
				startActivity(route);
			}
		});
		
		
	}

}
